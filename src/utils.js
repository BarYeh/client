export const defined = (thing) => {
    return thing !== undefined && thing !== null;
}